ARG DOCKER_FROM
FROM $DOCKER_FROM

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
      gettext-base \
      git \
      jq \
      make \
      moreutils \
      python3-pip \
      python3-venv \
    && rm -fr /var/lib/apt/lists/* \
    && rm -fr /usr/share/man/* /usr/share/doc/* /usr/share/info/* \
    ;


